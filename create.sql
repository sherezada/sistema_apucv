

DROP TABLE IF EXISTS usuario;
DROP TABLE IF EXISTS libro;
DROP TABLE IF EXISTS usuario_libro;
DROP TABLE IF EXISTS usuario_solicitud;
DROP TABLE IF EXISTS modalidad;
DROP TABLE IF EXISTS condicion;



CREATE TABLE usuario (
	id INT NOT NULL AUTO_INCREMENT, 
	nombre VARCHAR(255), 
	apellido VARCHAR(255), 
	telefono VARCHAR(20),
	correo VARCHAR(255) NOT NULL ,
	clave VARCHAR(255) NOT NULL,
	admin tinyint(1) NOT NULL DEFAULT 0,
	PRIMARY KEY (`id`)
);


CREATE TABLE modalidad (
	id INT NOT NULL AUTO_INCREMENT,
	descripcion VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`)
);


CREATE TABLE condicion (
	id INT NOT NULL AUTO_INCREMENT,
	descripcion VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE libro (
	id INT NOT NULL AUTO_INCREMENT, 
	titulo VARCHAR(255) NOT NULL ,
	autor VARCHAR(255) , 
	editorial VARCHAR (255), 
	condicion_id INT NOT NULL , 
	modalidad_id INT NOT NULL , 
	ano VARCHAR(50),
	solicitado tinyint(1) NOT NULL DEFAULT 0,
	disponible tinyint(1) NOT NULL DEFAULT 1,
	fecha_inicio DATE NOT NULL,
	usuario_id INT,
	precio REAL,
	PRIMARY KEY (`id`),
	CONSTRAINT `fk_libro_condicion_id`
    FOREIGN KEY (`condicion_id`)
    REFERENCES `condicion` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
    CONSTRAINT `fk_libro_modalidad_id`
    FOREIGN KEY (`modalidad_id`)
    REFERENCES `modalidad` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
    CONSTRAINT `fk_libro_usuario_id`
    FOREIGN KEY (`usuario_id`)
    REFERENCES `usuario` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);


CREATE TABLE usuario_libro (
	id INT AUTO_INCREMENT,
	libro_id INT NOT NULL ,
	usuario_id  INT NOT NULL ,
	fecha_solicitud DATE NOT NULL,
	nombre_libros_intercambio VARCHAR(255),
	editorial_libro_intercambio VARCHAR(200),
	autor_libro_intercambio VARCHAR(200),
	ano_libro_intercambio VARCHAR(20),
	PRIMARY KEY (`id`),
    CONSTRAINT `fk_usuario_solicitud_usuario_id`
    FOREIGN KEY (`usuario_id`)
    REFERENCES `usuario` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
    CONSTRAINT `fk_usuario_solicitud_libro_id`
    FOREIGN KEY (`libro_id`)
    REFERENCES `libro` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

