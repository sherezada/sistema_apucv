class Usuario < ActiveRecord::Base

	has_many :usuario_libro 
	has_many :libro, through: :usuario_libro

	def self.md5(clave) # función que permite encriptar las claves a MD5
		Digest::MD5.hexdigest("#{clave}")
	end

end
