class Libro < ActiveRecord::Base
	belongs_to :modalidad
	belongs_to :condicion

	has_many :usuario_libro 
	has_many :usuario, through: :usuario_libro
end
