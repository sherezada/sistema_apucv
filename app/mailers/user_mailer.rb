class UserMailer < ActionMailer::Base
  default from: "librosceapuv@gmail.com"

    def clave_enviada(usuario, clave) # correro que notifica el registro exitoso de un instructor e indica su clave para ingresar al sistema
		@usuario= usuario # instructor al que se le envía el correo
		@clave= clave # se envpia la clave generada por el sistema
		mail(to: usuario.correo, subject: 'Clave de ingreso para el Sistema de Negociacion de Libros') # esta instrucción indica a que correo envía la notificación
  	end
  
  def olvido_clave(usuario, clave) #correo que notifica el cambio del clave de un instructor para entrar al sistema
	@usuario= usuario # instructor al que se le envía el correo
	@clave= clave # se envía la clave nueva
	mail(to: usuario.correo, subject: 'Cambio de clave de ingreso para el Sistema de Negociacion de Libros') # esta instrucción indica a que correo envía la notificación
  end

  def solicitud_libro(usuario, libro)
  	@usuario= usuario
  	@libro= libro
  	mail(to: usuario.correo,subject: 'Han solicitado tu libro en el Sistema de Negociacion de Libros')
  end

  def acepta_solicitud(usuariosol, usuariores,libro)
  	@usuariores= usuariores
  	@usuariosol= usuariosol
  	@libro= libro
  	mail(to: usuariores.correo, subject: 'Datos del Solicitante de Tu Libro en el Sistema de Negociacion de Libros')
  end

   def acepta_solicitud2(usuariosol, usuariores,libro)
  	@usuariores= usuariores
  	@usuariosol= usuariosol
  	@libro= libro
  	mail(to: usuariosol.correo, subject: 'Datos del Responsable del Libro que negociaste en el Sistema de Negociacion de Libros')
  end

   def rechaza_solicitud(usuario,libro)
  	@usuario= usuario
  	@libro= libro
  	mail(to: usuario.correo, subject: 'Han rechazado tu solicitud en el Sistema de Negociacion de Libros')
  end

end
