class ModalidadsController < ApplicationController
  before_action :set_modalidad, only: [:show, :edit, :update, :destroy]

  

  # GET /modalidads/new
  def new
    @modalidad = Modalidad.new
  end

  # GET /modalidads/1/edit
  def edit
  end

  # POST /modalidads
  # POST /modalidads.json
  def create
    @modalidad = Modalidad.new(modalidad_params)

    respond_to do |format|
      if @modalidad.save
        format.html { redirect_to @modalidad, notice: 'Modalidad was successfully created.' }
        format.json { render action: 'show', status: :created, location: @modalidad }
      else
        format.html { render action: 'new' }
        format.json { render json: @modalidad.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /modalidads/1
  # PATCH/PUT /modalidads/1.json
  def update
    respond_to do |format|
      if @modalidad.update(modalidad_params)
        format.html { redirect_to @modalidad, notice: 'Modalidad was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @modalidad.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /modalidads/1
  # DELETE /modalidads/1.json
  def destroy
    @modalidad.destroy
    respond_to do |format|
      format.html { redirect_to modalidads_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_modalidad
      @modalidad = Modalidad.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def modalidad_params
      params.require(:modalidad).permit(:id_modalidad, :descripcion)
    end
end
