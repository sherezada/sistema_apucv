class UsuarioLibrosController < ApplicationController
  before_action :set_usuario_libro, only: [:show, :edit, :update, :destroy]


  # GET /usuario_libros/new
  def new
    @usuario_libro = UsuarioLibro.new
  end

  # GET /usuario_libros/1/edit
  def edit
  end

  # POST /usuario_libros
  # POST /usuario_libros.json
  def create
    @usuario_libro = UsuarioLibro.new(usuario_libro_params)

    respond_to do |format|
      if @usuario_libro.save
        format.html { redirect_to @usuario_libro, notice: 'Usuario libro was successfully created.' }
        format.json { render action: 'show', status: :created, location: @usuario_libro }
      else
        format.html { render action: 'new' }
        format.json { render json: @usuario_libro.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /usuario_libros/1
  # PATCH/PUT /usuario_libros/1.json
  def update
    respond_to do |format|
      if @usuario_libro.update(usuario_libro_params)
        format.html { redirect_to @usuario_libro, notice: 'Usuario libro was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @usuario_libro.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /usuario_libros/1
  # DELETE /usuario_libros/1.json
  def destroy
    @usuario_libro.destroy
    respond_to do |format|
      format.html { redirect_to usuario_libros_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_usuario_libro
      @usuario_libro = UsuarioLibro.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def usuario_libro_params
      params.require(:usuario_libro).permit(:id_libro, :id_usuario)
    end
end
