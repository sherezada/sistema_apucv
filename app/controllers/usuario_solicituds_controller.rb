class UsuarioSolicitudsController < ApplicationController
  before_action :set_usuario_solicitud, only: [:show, :edit, :update, :destroy]



  # GET /usuario_solicituds/new
  def new
    @usuario_solicitud = UsuarioSolicitud.new
  end

  # GET /usuario_solicituds/1/edit
  def edit
  end

  # POST /usuario_solicituds
  # POST /usuario_solicituds.json
  def create
    @usuario_solicitud = UsuarioSolicitud.new(usuario_solicitud_params)

    respond_to do |format|
      if @usuario_solicitud.save
        format.html { redirect_to @usuario_solicitud, notice: 'Usuario solicitud was successfully created.' }
        format.json { render action: 'show', status: :created, location: @usuario_solicitud }
      else
        format.html { render action: 'new' }
        format.json { render json: @usuario_solicitud.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /usuario_solicituds/1
  # PATCH/PUT /usuario_solicituds/1.json
  def update
    respond_to do |format|
      if @usuario_solicitud.update(usuario_solicitud_params)
        format.html { redirect_to @usuario_solicitud, notice: 'Usuario solicitud was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @usuario_solicitud.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /usuario_solicituds/1
  # DELETE /usuario_solicituds/1.json
  def destroy
    @usuario_solicitud.destroy
    respond_to do |format|
      format.html { redirect_to usuario_solicituds_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_usuario_solicitud
      @usuario_solicitud = UsuarioSolicitud.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def usuario_solicitud_params
      params.require(:usuario_solicitud).permit(:id_libro, :id_usuario, :fecha_solicitud)
    end
end
