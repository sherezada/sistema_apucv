class SesionController < ApplicationController

  layout "encabezado"

  def index
    
  end

  def validar_sesion
    reset_session   
    contrasena = params[:password]
    clave = Usuario.md5(contrasena)
    usuario = Usuario.where(correo: params[:correo]).take
    if usuario && clave == usuario.clave 
      session[:logueado] = true
      session[:identificador] = usuario.id
      session[:nombre] = usuario.nombre
      session[:apellido] = usuario.apellido
      session[:id] = usuario.id
      if usuario.admin!=true
        redirect_to controller: :gestion, action: :index
      return
      
      else
        redirect_to action: :indexadmin
      end
    else
      #si se mete por aqui es porque no coincidio la clave
      flash[:mensaje] = "El correo electrónico o la contraseña son incorrectos"
      redirect_to controller: :sesion, action: :index
      return
    end
  end

  def elimuser
    @usuarios= Usuario.all
    render 'eimuser'
  end


  def olvidoc
    @usuario= Usuario.where(correo: params[:correo]).take
    @clave= random_alphanumeric
    if @usuario!=nil
      @usuario.clave = Usuario.md5(@clave)
      @clave2= @usuario.clave
        if @usuario.update(:clave=>@clave2)
          UserMailer.olvido_clave(@usuario,@clave).deliver
          flash[:mensaje]= "Su clave fue reestablecida satisfactoriamente"
          redirect_to controller: :sesion, action: :index
        else
          format.html { render action: 'edit' }
          format.json { render json: @usuario.errors, status: :unprocessable_entity }
        end
    else
      flash[:mensaje]= "El correo que instrodujo no es correcto, por favor verifique"
      redirect_to controller: :sesion, action: :olvido
    end
  end

  def random_alphanumeric(size=8) # función generado de claves aleatorias
    chars = ('a'..'z').to_a + ('A'..'Z').to_a + (0..9).to_a #puede contener letras mayusculas y minusculas, ademas de los numeros
    @clave=(0...size).collect { chars[Kernel.rand(chars.length)] }.join
    return @clave
  end

  def cerrar_sesion
    reset_session
    redirect_to controller: :sesion, action: :index
    return
  end

 

end
