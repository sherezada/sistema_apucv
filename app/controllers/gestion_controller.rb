class GestionController < ApplicationController

	layout "logueado"
	def index
		@libros = Libro.where("usuario_id != ? and disponible = ?", session[:identificador], true)
	end

	def ver_detalles
		@libro = Libro.find(params[:id])
		@responsable = Usuario.find(@libro.usuario_id)
		if @libro.modalidad.id == 1
			@seleccion = Libro.where(usuario_id:session[:identificador],modalidad_id:1)
		end
	end

	def solicitar_libro
		flash[:mensaje] = false
		@libros = Libro.find(params[:id])
		@responsable= Usuario.find(@libros.usuario_id)
		if !UsuarioLibro.where(libro_id:@libros.id,usuario_id:session[:identificador]).take
			t = Time.now
			date = t.year.to_s+"-"+t.month.to_s+"-"+t.day.to_s
			if @libros.modalidad.id !=1
				UsuarioLibro.create(libro_id:params[:id],usuario_id:session[:identificador],fecha_solicitud:date,nombre_libros_intercambio:nil, editorial_libro_intercambio:nil, autor_libro_intercambio:nil, ano_libro_intercambio:nil)
			else
				UsuarioLibro.create(libro_id:params[:id],usuario_id:session[:identificador],fecha_solicitud:date,nombre_libros_intercambio:params[:libro_intercambio],editorial_libro_intercambio: @libros.editorial, autor_libro_intercambio: @libros.autor,ano_libro_intercambio: @libros.ano)
			end
			@libros.solicitado = true
			@libros.save
			UserMailer.solicitud_libro(@responsable, @libros).deliver
			flash[:mensaje] = "Se ha enviado la información sobre su solicitud al responsable del libro, se le enviara por correo la información sobre el tramite"
			redirect_to controller: :gestion , action: :index
		else
			flash[:mensaje] = "Usted ya ha solicitado este libro"
			redirect_to controller: :gestion , action: :index
		end		
	end

	def mis_solicitudes
		@solicitudes = Libro.where(usuario_id:session[:identificador],solicitado:true)
	end

	def mis_solicitudes_enviadas
		@solicitudes = UsuarioLibro.where(usuario_id:session[:identificador])
	end

	def rechazar
		@usuariosol= Usuario.find(params[:id_persona])
		@libro= Libro.find(params[:id_libro])
		UserMailer.rechaza_solicitud(@usuariosol,@libro).deliver
		UsuarioLibro.where(usuario_id:params[:id_persona],libro_id:params[:id_libro]).delete_all
		redirect_to controller: :gestion , action: :mis_solicitudes
	end

	def aceptar
		@usuariores= Usuario.find(session[:id])
		@usuariosol= Usuario.find(params[:id_persona])
		@libro= Libro.find(params[:id_libro])
		@libro.disponible= 0
		@libro.save
		UsuarioLibro.where(usuario_id:params[:id_persona],libro_id:params[:id_libro]).delete_all
		UserMailer.acepta_solicitud(@usuariosol,@usuariores,@libro).deliver
		UserMailer.acepta_solicitud2(@usuariosol,@usuariores,@libro).deliver
		redirect_to controller: :gestion , action: :mis_solicitudes
	end

	def agregarl
        @condiciones= Condicion.all
        @modalidades= Modalidad.all
        render 'agregarlibro'
    end

    def editarl
        @libros= Libro.where(usuario_id:session[:id],disponible:true,solicitado:false)
        render 'editarvista'
    end

    def editar_libro
        @libro= Libro.find(params[:id])
        @condiciones= Condicion.all
        @modalidades= Modalidad.all
        render 'editarlibro'
    end

    def update
         @libro= Libro.find(params[:id])
         @libro.titulo= params[:titulo]
         @libro.autor= params[:autor]
         @libro.editorial= params[:editorial]
         @libro.condicion_id= params[:condicion]
         @libro.modalidad_id= params[:modalidad]
         @libro.ano= params[:ano]
        if @libro.save
          flash[:mensaje]= "El libro fue editado satisfactoriamente"
          redirect_to controller: :gestion, action: :editarl
        else
          flash[:mensaje]= "Ocurrio un error al actualizar el libro, por favor intentelo de nuevo"
          redirect_to controller: :gestion, action: :editarl
        end
      end

      def eliminarl
          @libros= Libro.where(usuario_id:session[:id],disponible:true,solicitado:false)
          render 'eliminar'
      end

      def eliminar_libro
          @libro=Libro.find(params[:id])
          if @libro.destroy
              flash[:mensaje]= "El libro fue eliminado con exito"
              redirect_to controller: :gestion, action: :eliminarl
          else
              flash[:mensaje]= "El libro no fue eliminado, por favor intentelo de nuevo"
              redirect_to controller: :gestion, action: :eliminarl
          end
      end

	# va en el controlador sesion

	def cambiar_clave
		flash[:mensaje] = false
		if params[:password1] == params[:password2]
			@usuario = Usuario.find(session[:identificador])
			@usuario.clave = Usuario.md5(params[:password1])
			@usuario.save
			flash[:mensaje] = "Su clave ha sido cambiada con éxito"
		else
			flash[:mensaje_error] = "Las claves deben coincidir"
		end
		redirect_to controller: :gestion , action: :index
	end


end