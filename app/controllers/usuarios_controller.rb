class UsuariosController < ApplicationController
  #before_action :set_usuario, only: [:show, :edit, :update, :destroy]

  # GET /usuarios
  # GET /usuarios.json
 

  # GET /usuarios/1
  # GET /usuarios/1.json


  # GET /usuarios/new
  def new
    @usuario = Usuario.new
  end

  # GET /usuarios/1/edit
  def edit
  end

  def random_alphanumeric(size=8) # función generado de claves aleatorias
    chars = ('a'..'z').to_a + ('A'..'Z').to_a + (0..9).to_a #puede contener letras mayusculas y minusculas, ademas de los numeros
    @clave=(0...size).collect { chars[Kernel.rand(chars.length)] }.join
    return @clave
  end

  # POST /usuarios
  # POST /usuarios.json
  def create
    @usuario = Usuario.new
    @usuario.nombre = params[:nombre]
    @usuario.apellido = params[:apellido]
    @usuario.telefono = params[:telefono]
    @usuario.correo = params[:correo]
    @clave= random_alphanumeric
    @usuario.clave = Usuario.md5(@clave)
    @usuario.admin = 0

    respond_to do |format|
      if @usuario.save
        UserMailer.clave_enviada(@usuario, @clave).deliver
        flash[:mensaje]= "El usuario fue creado satisfactoriamente"
        format.html { redirect_to controller: :sesion, action: :indexadmin, notice: 'El usuario fue creado satisfactoriamente' }
      else
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /usuarios/1
  # PATCH/PUT /usuarios/1.json
  def update
    respond_to do |format|
      if @usuario.update(usuario_params)
        format.html { redirect_to @usuario, notice: 'Usuario was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /usuarios/1
  # DELETE /usuarios/1.json
  def destroy
    @usuario = Usuario.find(params[:id])
    @usuario.destroy
    respond_to do |format|
      flash[:mensaje] = "El usuario fue eliminado satisfactoriamente"
      format.html { redirect_to controller: :sesion, action: :elimuser, mensaje: "EL usuario fue eliminado satisfactoriamente" }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_usuario
      @usuario = Usuario.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    #def usuario_params
      #params.require(:usuario).permit(:id, :nombre, :telefono, :correo, :login, :clave)
    #end
end
