class LibrosController < ApplicationController
  before_action :set_libro, only: [:show, :edit, :update, :destroy]

  # GET /libros
  # GET /libros.json
  def index
    @libros = Libro.all
  end

  # GET /libros/1
  # GET /libros/1.json
  def show
  end

  # GET /libros/new
  def new
    @libro = Libro.new
  end

  # GET /libros/1/edit
  def edit
  end

  # POST /libros
  # POST /libros.json
  def create
    flash[:mensaje]= false
    @libro = Libro.new
    @libro.titulo= params[:titulo]
    @libro.autor= params[:autor]
    @libro.editorial= params[:editorial]
    @libro.condicion_id= params[:condicion]
    @libro.modalidad_id= params[:modalidad]
    @libro.ano= params[:ano]
    @libro.solicitado= 0
    @libro.disponible= 1
    t = Time.now
    date = t.year.to_s+"-"+t.month.to_s+"-"+t.day.to_s
    @libro.fecha_inicio= date
    @libro.usuario_id= session[:id]
    if @libro.save
      flash[:mensaje]= "El libro fue agregado satisfactoriamente"
      redirect_to controller: :gestion, action: :agregarl
    else
      flash[:mensaje]="Ocurrio un error creando el libro, por favor intentelo de nuevo"
      redirect_to controller: :gestion, action: :agregarl
    end
  end

  # PATCH/PUT /libros/1
  # PATCH/PUT /libros/1.json


  # DELETE /libros/1
  # DELETE /libros/1.json
  def destroy
    @libro.destroy
    respond_to do |format|
      format.html { redirect_to libros_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_libro
      @libro = Libro.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
end
