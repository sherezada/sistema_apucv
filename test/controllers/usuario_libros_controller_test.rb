require 'test_helper'

class UsuarioLibrosControllerTest < ActionController::TestCase
  setup do
    @usuario_libro = usuario_libro(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:usuario_libro)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create usuario_libro" do
    assert_difference('UsuarioLibro.count') do
      post :create, usuario_libro: { id_libro: @usuario_libro.id_libro, id_usuario: @usuario_libro.id_usuario }
    end

    assert_redirected_to usuario_libro_path(assigns(:usuario_libro))
  end

  test "should show usuario_libro" do
    get :show, id: @usuario_libro
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @usuario_libro
    assert_response :success
  end

  test "should update usuario_libro" do
    patch :update, id: @usuario_libro, usuario_libro: { id_libro: @usuario_libro.id_libro, id_usuario: @usuario_libro.id_usuario }
    assert_redirected_to usuario_libro_path(assigns(:usuario_libro))
  end

  test "should destroy usuario_libro" do
    assert_difference('UsuarioLibro.count', -1) do
      delete :destroy, id: @usuario_libro
    end

    assert_redirected_to usuario_libros_path
  end
end
