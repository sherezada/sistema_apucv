require 'test_helper'

class UsuarioSolicitudsControllerTest < ActionController::TestCase
  setup do
    @usuario_solicitud = usuario_solicitud(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:usuario_solicitud)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create usuario_solicitud" do
    assert_difference('UsuarioSolicitud.count') do
      post :create, usuario_solicitud: { fecha_solicitud: @usuario_solicitud.fecha_solicitud, id_libro: @usuario_solicitud.id_libro, id_usuario: @usuario_solicitud.id_usuario }
    end

    assert_redirected_to usuario_solicitud_path(assigns(:usuario_solicitud))
  end

  test "should show usuario_solicitud" do
    get :show, id: @usuario_solicitud
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @usuario_solicitud
    assert_response :success
  end

  test "should update usuario_solicitud" do
    patch :update, id: @usuario_solicitud, usuario_solicitud: { fecha_solicitud: @usuario_solicitud.fecha_solicitud, id_libro: @usuario_solicitud.id_libro, id_usuario: @usuario_solicitud.id_usuario }
    assert_redirected_to usuario_solicitud_path(assigns(:usuario_solicitud))
  end

  test "should destroy usuario_solicitud" do
    assert_difference('UsuarioSolicitud.count', -1) do
      delete :destroy, id: @usuario_solicitud
    end

    assert_redirected_to usuario_solicituds_path
  end
end
